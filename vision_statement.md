"The [Controller Manager](https://control.ros.org/master/doc/getting_started/getting_started.html#controller-manager) (CM) connects the controllers and hardware-abstraction sides of the ros2_control framework. It also serves as the entry-point for users via ROS services. The CM implements a node without an executor so that it can be integrated into a custom setup. However, it’s usually recommended to use the default node-setup implemented in [ros2_control_node](https://github.com/ros-controls/ros2_control/blob/master/controller_manager/src/ros2_control_node.cpp) file from the `controller_manager` package. This manual assumes that you use this default node-setup.

On the one hand, CM manages (e.g. loads, activates, deactivates, unloads) controllers and the interfaces they require. On the other hand, it has access (via the Resource Manager) to the hardware components, i.e. their interfaces. The Controller Manager matches _required_ and _provided_ interfaces, granting controllers access to hardware when enabled, or reporting an error if there is an access conflict.

The execution of the control-loop is managed by the CM’s `update()` method. It reads data from the hardware components, updates outputs of all active controllers, and writes the result to the components."


ros2 controll documentation. section: Getting started.
https://control.ros.org/master/doc/getting_started/getting_started.html

Our own controller manager is very similar to the ros2 control concepts for this type of software. It will currently control 2 different controllers:
- The drive_controller, which translates the path messages to the robot's main drive. It will follow the concepts of ros2 nav2.
- the move_controller, which translates the messages for the actuators. It will follow the concepts of ros2 moveit.